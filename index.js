// console.log(1234);

// Bài 1: Xuất 3 số theo thứ tựu tăng dần

document.getElementById("sapXep").onclick = function () {
  var soThu1 = document.getElementById("soThu1").value * 1;
  //   console.log("soThu1: ", soThu1);

  var soThu2 = document.getElementById("soThu2").value * 1;
  //   console.log("soThu2: ", soThu2);

  var soThu3 = document.getElementById("soThu3").value * 1;
  //   console.log("soThu3: ", soThu3);

  var thuTuSapXep;

  if (soThu1 > soThu2 && soThu1 > soThu3 && soThu2 > soThu3) {
    thuTuSapXep = soThu3 + "<" + soThu2 + "<" + soThu1;
    // sothu3 < sothu2 < sothu1
  } else if (soThu1 > soThu2 && soThu1 > soThu3 && soThu3 > soThu2) {
    thuTuSapXep = soThu2 + "<" + soThu3 + "<" + soThu1;
    // sothu2 < sothu3 < sothu1
  } else if (soThu2 > soThu1 && soThu2 > soThu3 && soThu1 > soThu3) {
    thuTuSapXep = soThu3 + "<" + soThu1 + "<" + soThu2;
    // sothu3 < sothu1 < sothu2
  } else if (soThu2 > soThu1 && soThu2 > soThu3 && soThu3 > soThu1) {
    thuTuSapXep = soThu1 + "<" + soThu3 + "<" + soThu2;
    // sothu1 < sothu3 < sothu2
  } else if (soThu3 > soThu1 && soThu3 > soThu2 && soThu2 > soThu1) {
    thuTuSapXep = soThu1 + "<" + soThu2 + "<" + soThu3;
    // sothu1 < sothu2 < sothu3
  } else if (soThu3 > soThu1 && soThu3 > soThu2 && soThu1 > soThu2) {
    thuTuSapXep = soThu2 + "<" + soThu1 + "<" + soThu3;
    // sothu2 < sothu1 < sothu3
  }
  document.getElementById("thuTuSapXep").innerHTML = thuTuSapXep;
};

// BÀi 2: Gửi lời chào

document.getElementById("guiLoiChao").onclick = function () {
  var chonThanhVien = document.getElementById("chonThanhVien").value;
  //   console.log("chonThanhVien: ", chonThanhVien);
  document.getElementById("loiChao").innerHTML =
    "Xin chào " + chonThanhVien + "!";
};

// BÀi 3: Đếm số chẵn lẻ

kiemTraSoChan = function (a) {
  if (a % 2 == 0) {
    return 0;
  } else {
    return 1;
  }
};

document.getElementById("demSoChanLe").onclick = function () {
  var countSoLe = 0;

  var soThu1 = document.getElementById("soThu1-ChanLe").value * 1;
  // console.log("soThu1: ", soThu1);

  var soThu2 = document.getElementById("soThu2-ChanLe").value * 1;
  var soThu3 = document.getElementById("soThu3-ChanLe").value * 1;

  countSoLe =
    kiemTraSoChan(soThu1) + kiemTraSoChan(soThu2) + kiemTraSoChan(soThu3);

  //   console.log("countSoLe: ", countSoLe);
  document.getElementById("soChanSoLe").innerHTML =
    "Có " + (3 - countSoLe) + " số chẵn, " + countSoLe + " số lẻ";
};

// document.getElementById("demSoChanLe").onclick = function () {
//   var soThu1 = document.getElementById("soThu1-ChanLe").value * 1;
//   var soThu2 = document.getElementById("soThu2-ChanLe").value * 1;
//   var soThu3 = document.getElementById("soThu3-ChanLe").value * 1;

//   var countSoLe = 0;

//   if (soThu1 % 2 == 1) {
//     countSoLe++;
//   }
//   if (soThu2 % 2 == 1) {
//     countSoLe++;
//   }
//   if (soThu3 % 2 == 1) {
//     countSoLe++;
//   }
//   document.getElementById("soChanSoLe").innerHTML =
//     "Có " + (3 - countSoLe) + " số chẵn, " + countSoLe + " số lẻ";
// };

// BÀI 4: Đoán hình tam giác
xacDinhLoaiTamGiac = function (a, b, c) {
  if (a == b && a == c && b == c) {
    return "đều";
  } else if (
    a * a == b * b + c * c ||
    b * b == a * a + c * c ||
    c * c == a * a + b * b
  ) {
    if (a == b || a == c || b == c) {
      return "vuông cân";
    } else {
      return "vuông";
    }
  } else if (a == b || b == c || c == a) {
    return "cân";
  } else {
    return "bất kỳ";
  }
};

document.getElementById("duDoan").onclick = function () {
  //   alert(234);`
  var loaiTamGiac = "";

  var doDaiCanh1 = document.getElementById("doDaiCanh1").value * 1;
  //   console.log("doDaiCanh1: ", doDaiCanh1);
  var doDaiCanh2 = document.getElementById("doDaiCanh2").value * 1;
  //   console.log("doDaiCanh2: ", doDaiCanh2);
  var doDaiCanh3 = document.getElementById("doDaiCanh3").value * 1;
  //   console.log("doDaiCanh3: ", doDaiCanh3);

  loaiTamGiac = xacDinhLoaiTamGiac(doDaiCanh1, doDaiCanh2, doDaiCanh3);
  document.getElementById("ketQuaDuDoan").innerHTML =
    "Hình tam giác " + loaiTamGiac;
};

// BÀI 5: Tính ngày tháng năm

// console.log("yes");

//hàm tính số ngày trong tháng
timSoNgayTrongThang_Nam = function (y, z) {
  if (y > 12 || y < 1) {
    // return"bạn đùa tôi à, chỉ có từ tháng 1 đến tháng 12 thôi";
    return -Infinity;
  } else {
    switch (y) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: {
        return 31;
      }
      case 4:
      case 6:
      case 9:
      case 11: {
        return 30;
      }
      case 2: {
        if (z % 4 == 0) {
          return 29;
        }
        return 28;
      }
    }
  }
};

// console.log("số ngày là:", timSoNgayTrongThang_Nam(2, 2020));
// console.log("diết");

// hàm trả kết quả ngày nhập

xuatNgayThangNam = function (x, y, z) {
  if (y > 12 || y < 1) {
    return "Ở Trái Đất chỉ có từ tháng 1 đến tháng 12 thôi. Bạn đến từ Sao Hoả à?";
  } else if (x > timSoNgayTrongThang_Nam(y, z) || x < 1) {
    return "bạn đùa tôi à, trong tháng này làm gì có ngày này  ";
  } else {
    var xuatNgayThangNam = "ngày " + x + " tháng " + y + " năm " + z;
    return xuatNgayThangNam;
  }
};
// console.log("xuatNgayThangNam: ", xuatNgayThangNam(29, 2, 2020));

// hàm xuất ngày hôm qua
document.getElementById("ngayHomQua").onclick = function () {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;

  if (ngay > timSoNgayTrongThang_Nam(thang, nam) || ngay < 1) {
    ngay = ngay;
  } else {
    if (ngay == 1) {
      if (thang == 1) {
        ngay = timSoNgayTrongThang_Nam(12, nam);
        thang = 12;
        nam = nam - 1;
        // cái trên xử lý cho ngày hôm qua của ngày đầu năm
      } else {
        ngay = timSoNgayTrongThang_Nam(thang - 1, nam);
        thang = thang - 1;
        // cái trên xử lý cho ngày hôm qua của ngày đầu tháng (tháng #1)
      }
    } else {
      ngay = ngay - 1;
    }
  }
  // console.log("xuatNgayThangNam: ", xuatNgayThangNam(ngay, thang, nam));
  // console.log("ngày:", ngay);
  // console.log("tháng:", thang);
  // console.log("năm:", nam);

  document.getElementById("ngayThangNam").innerHTML = xuatNgayThangNam(
    ngay,
    thang,
    nam
  );
};

// // hàm xuất ngày mai
document.getElementById("ngayMai").onclick = function () {
  var ngay = document.getElementById("ngay").value * 1;
  var thang = document.getElementById("thang").value * 1;
  var nam = document.getElementById("nam").value * 1;

  if (ngay > timSoNgayTrongThang_Nam(thang, nam) || ngay < 1) {
    ngay = ngay;
  } else {
    switch (thang) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10: {
        if (ngay == 31) {
          ngay = 1;
          thang = thang + 1;
        } else {
          ngay = ngay + 1;
        }
        break;
      }
      case 4:
      case 6:
      case 9:
      case 11: {
        if (ngay == 30) {
          ngay = 1;
          thang = thang + 1;
        } else {
          ngay = ngay + 1;
        }
        break;
      }
      case 2: {
        if (ngay >= 28) {
          ngay = 1;
          thang = thang + 1;
        } else {
          ngay = ngay + 1;
        }
        break;
      }
      // cái trên xử lý cho ngày mai của ngày cuối tháng ( tháng #12
      case 12: {
        if (ngay == 31) {
          ngay = 1;
          thang = 1;
          nam = nam + 1;
        } else {
          ngay = ngay + 1;
        }
        break;

        // cái trên xử lý cho ngày mai của ngày cuối năm
      }
    }
  }
  // console.log("ngày:", ngay);
  // console.log("tháng:", thang);
  // console.log("năm:", nam);
  // console.log("xuatNgayThangNam: ", xuatNgayThangNam(ngay, thang, nam));
  document.getElementById("ngayThangNam").innerHTML = xuatNgayThangNam(
    ngay,
    thang,
    nam
  );
};

// BÀI 6: Đến số ngày trong tháng

// em làm hàm tính số ngày trong tháng ở bài 5 rùi

document.getElementById("tinhSoNgayTrongThang").onclick = function () {
  // alert(123)
  var thang_bai6 = document.getElementById("thang_bai6").value * 1;
  var nam_bai6 = document.getElementById("nam_bai6").value * 1;

  document.getElementById("soNgayTrongThang").innerHTML =
    "Tháng " +
    thang_bai6 +
    " năm " +
    nam_bai6 +
    " có " +
    timSoNgayTrongThang_Nam(thang_bai6, nam_bai6) +
    " ngày";
};

// BÀI 7: Đọc số

// hàm đọc số
docSoHangTram = function (x) {
  switch (x) {
    case 1: {
      return "một";
    }
    case 2: {
      return "hai";
    }
    case 3: {
      return "ba";
    }
    case 4: {
      return "bốn";
    }
    case 5: {
      return "năm";
    }
    case 6: {
      return "sáu";
    }
    case 7: {
      return "bảy";
    }
    case 8: {
      return "tám";
    }
    case 9: {
      return "chín";
    }
  }
};
// console.log("docSoHangTram: ", docSoHangTram(3));
docSoHangChuc = function (x, y) {
  switch (x) {
    case 0: {
      return "linh";
    }
    case 1: {
      return "mười";
    }
    case 2: {
      return "hai mươi";
    }
    case 3: {
      return "ba mươi";
    }
    case 4: {
      return "bốn mươi";
    }
    case 5: {
      return "năm mươi";
    }
    case 6: {
      return "sáu mươi";
    }
    case 7: {
      return "bảy mươi";
    }
    case 8: {
      return "tám mươi";
    }
    case 9: {
      return "chín mươi";
    }
  }
};
// console.log("docSoHangChuc: ", docSoHangChuc(0));
docSoHangDonVi = function (x, y) {
  switch (y) {
    case 0: {
      return "";
    }
    case 1: {
      if (x <= 1) {
        return "một";
      } else return "mốt";
    }
    case 2: {
      return "hai";
    }
    case 3: {
      return "ba";
    }
    case 4: {
      return "tư";
    }
    case 5: {
      if (x == 0) {
        return "năm";
      } else return "lăm";
    }
    case 6: {
      return "sáu";
    }
    case 7: {
      return "bảy";
    }
    case 8: {
      return "tám";
    }
    case 9: {
      return "chín";
    }
  }
};
// console.log("docSoHangDonVi: ", docSoHangDonVi(0,7));

document.getElementById("docSo").onclick = function () {
  // alert(1234)

  var so3ChuSo = document.getElementById("so3ChuSo").value * 1;

  var soHangTram = Math.floor(so3ChuSo / 100);
  var soHangChuc = Math.floor(so3ChuSo / 10) % 10;
  var soHangDonVi = so3ChuSo % 10;

  var ketQuaDocSo = null;
  if (soHangTram > 9 || soHangTram == 0) {
    ketQuaDocSo = "thím ngáo à! nhập số có 3 chữ số thôi";
  } else {
    ketQuaDocSo =
      docSoHangTram(soHangTram) +
      " trăm " +
      docSoHangChuc(soHangChuc) +
      " " +
      docSoHangDonVi(soHangChuc, soHangDonVi);
  }
  console.log("soHangTram: ", soHangTram);
  console.log("soHangChuc: ", soHangChuc);
  console.log("soHangDonVi: ", soHangDonVi);

  // console.log(
  //   "số:",
  //   docSoHangTram(soHangTram) +
  //     " trăm " +
  //     docSoHangChuc(soHangChuc) +" "+
  //     docSoHangDonVi(soHangChuc,soHangDonVi)
  // );

  document.getElementById("ketQuaDocSo").innerHTML = ketQuaDocSo;
};

// BÀi 8: Tìm sinh viên xa trường nhất

// hàm tính khoảng cách giữa 2 điểm có toạ độ (x,y) và (a,b)
tinhKhoangCach = function (x, y, a, b) {
  return Math.sqrt((x - a) * (x - a) + (y - b) * (y - b));
};

// console.log("tinhKhoangCach: ", tinhKhoangCach(4, 5, 1, 1));

//hàm tìm số lớn nhất trong 3 số a,b,c
timSoLonNhat = function (a, b, c) {
  if (a >= b && a >= c) {
    return a;
  } else if (b >= a && b >= c) {
    return b;
  } else if (c >= a && c >= b) {
    return c;
  }
};
// console.log("timSoLonNhat: ", timSoLonNhat(6, 8, 8));

//hàm so sánh
document.getElementById("tim").onclick = function () {
  var x0 = document.getElementById("x0").value * 1;
  var y0 = document.getElementById("y0").value * 1;

  var x1 = document.getElementById("x1").value * 1;
  var y1 = document.getElementById("y1").value * 1;
  var tenSV1 = document.getElementById("tenSV1").value;
  // console.log("tenSV1: ", tenSV1);

  var x2 = document.getElementById("x2").value * 1;
  var y2 = document.getElementById("y2").value * 1;
  var tenSV2 = document.getElementById("tenSV2").value;
  // console.log("tenSV2: ", tenSV2);

  var x3 = document.getElementById("x3").value * 1;
  var y3 = document.getElementById("y3").value * 1;
  var tenSV3 = document.getElementById("tenSV3").value;
  // console.log("tenSV3: ", tenSV3);

  var d1 = tinhKhoangCach(x1, y1, x0, y0);
  var d2 = tinhKhoangCach(x2, y2, x0, y0);
  var d3 = tinhKhoangCach(x3, y3, x0, y0);

  // console.log("d1: ", d1);
  // console.log("d2: ", d2);
  // console.log("d3: ", d3);

  // var d1 = 1;
  // var d2 = 4;
  // var d3 = 2;

  var max = timSoLonNhat(d1, d2, d3);
  // console.log("max: ", max);

  var ketquaTim = null;

  switch (max) {
    case d1: {
      ketquaTim = tenSV1;
      break;
    }

    case d2: {
      ketquaTim = tenSV2;
      break;
    }

    case d3: {
      ketquaTim = tenSV3;
      break;
    }
  }

  // console.log("ketquaTim: ", ketquaTim);

  document.getElementById("ketquaTim").innerHTML =
    "Sinh viên xa trường nhất: " + ketquaTim;
};
